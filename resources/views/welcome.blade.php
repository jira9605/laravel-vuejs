<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel Vue application</title>
    <link href="css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
    <h2 style="text-align: center"> Laravel and Vue application </h2>
    <div id="root">



        <products>

        </products>
   


</div>
<script src="js/app.js" ></script>
</body>
</html>
